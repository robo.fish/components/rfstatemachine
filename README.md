# RFStateMachine

A state machine based on `Hashable` types, usually `enum`s, for representing states and events,
and Combine publishers, usually `Future`s, that implement the state transitions.
