// swift-tools-version:5.3

import PackageDescription

let package = Package(
    name: "RFStateMachine",
    platforms: [
        .macOS(.v10_15),
        .iOS(.v14),
        .watchOS(.v7),
        .tvOS(.v14),
    ],
    products: [
        .library(
            name: "RFStateMachine",
            targets: ["RFStateMachine"]),
    ],
    targets: [
        .target(
            name: "RFStateMachine",
            dependencies: []),
        .testTarget(
            name: "RFStateMachineTests",
            dependencies: ["RFStateMachine"]),
    ]
)
