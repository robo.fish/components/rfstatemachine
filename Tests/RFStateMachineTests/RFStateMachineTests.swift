import XCTest
import Combine
@testable import RFStateMachine

final class RFStateMachineTests: XCTestCase
{
	static var allTests = [
		("testTransitions", testTransitions),
	]

	enum MachineState : String, CaseIterable
	{
		case idle = "Idle"
		case processing = "Processing"
		case waitingForInput = "Waiting for Input"
		case resultReady = "Result Ready"
	}

	enum MachineEvent
	{
		case startProcessing
		case input
	}

	typealias TestMachine = RFStateMachine<MachineState, MachineEvent>

	func testTransitions() throws {
		let expectation = XCTestExpectation(description:"Completed all transitions")
		let machine = TestMachine(initialState: .idle)
		let queue = DispatchQueue(label: "StateMachine test")
		machine.transitions = [
			.idle / .startProcessing  : (_operation(resultingState: .processing), .processing),
			.processing / nil         : (_operation(resultingState: .waitingForInput), .processing),
			.waitingForInput / .input : (_operation(resultingState: .resultReady), .processing),
			.resultReady / nil        : (nil, nil)
		]

		var transitionCheck = [MachineState:Bool]()
		for state in MachineState.allCases { transitionCheck[state] = false }

		let subscription = machine.$state.receive(on:queue).sink { state in
			print("visited state: \(state.rawValue)")
			// marking state as visited
			transitionCheck[state] = true
			if state == .waitingForInput {
				machine.react(to: .input)
			}
			// checking if all states were visited
			if transitionCheck.reduce(true, { $0 && $1.value } ) {
				expectation.fulfill()
			}
		}

		queue.async { machine.react(to: .startProcessing) }
		wait(for: [expectation], timeout: 1.0)
		subscription.cancel()
	}

	private func _operation(resultingState : MachineState) -> AnyPublisher<MachineState, Never> {
		Deferred {
			Just(resultingState)
		}.eraseToAnyPublisher()
	}

}
