//
//  RFStateMachine.swift
//  RFStateMachine
//
//  Copyright 2020 Robo.Fish UG (haftungsbeschraenkt)
//

import Combine

public class RFStateMachine<State, Event> : ObservableObject where State : Hashable, Event : Hashable
{
	public struct Action : Hashable
	{
		public let state : State
		public let event : Event?

		public init(state : State, event : Event? = nil)
		{
			self.state = state
			self.event = event
		}
	}

	public typealias Reaction = (AnyPublisher<State, Never>?, State?)
	public typealias TransitionMap = [Action: Reaction]

	public var transitions : TransitionMap?

	private var _runningOperations = [AnyCancellable]()

	@Published public private(set) var state : State

	public init(initialState : State)
	{
		self.state = initialState
	}

	public func react(to event : Event?)
	{
		let action = Action(state: self.state, event: event)
		if let (reaction, transientState) = transitions?[action], reaction != nil
		{
			self.state = transientState ?? state
			let newOperation = reaction!.sink { newState in
				self.state = newState
				self.react(to: nil)
			}
			_runningOperations.append(newOperation)
		}
		else
		{
			// removing subscriptions to expired publishers
			_runningOperations.removeAll()
		}
	}

}

public func /<State,Event>(lhs : State, rhs : Event?) -> RFStateMachine<State,Event>.Action where State : Hashable, Event : Hashable
{
	RFStateMachine<State,Event>.Action(state: lhs, event: rhs)
}
